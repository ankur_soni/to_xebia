var app = angular.module('shoppingCartApp', ['ui.router','localStore']);

app.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider.state('home', {
            url: '/home',
            templateUrl: 'views/home.html'
            /*controller: 'mainController'*/
        })

        .state('formals', {
            url: '/formals',
            templateUrl: 'views/formals.html'
            /*controller: 'mainController'*/
        })

        .state('casuals', {
            url: '/casuals',
            templateUrl: 'views/casuals.html'
            /*controller: 'mainController'*/
        })

		.state('shoes', {
            url: '/shoes',
            templateUrl: 'views/shoes.html'
            /*controller: 'mainController'*/
        })

        .state('accessories', {
            url: '/accessories',
            templateUrl: 'views/accessories.html'
            /*controller: 'mainController'*/
        })

        .state('details',{
        	url: '/details/:product',
        	templateUrl: 'views/details.html',
           	controller: 'mainController'
        })

        .state('cart',{
        	url: '/cart',
        	templateUrl: 'views/cart.html',
            controller: 'cartController'
        })

        .state('cart.checkout',{
        	url: '/checkout',
        	templateUrl: 'views/contact-details.html',
            controller: 'cartController'
        })

        .state('confirm',{
        	url: '/confirm',
        	templateUrl: 'views/confirm.html',
            controller: 'cartController'
        })

        .state('retrieve',{
            url: '/retrieve',
            templateUrl: 'views/retrieveCart.html',
            controller: 'cartController'
        })

      

        /*.state('about', {
            url: '/about',
            views: {
                '': { templateUrl: 'views/about.html' },
                'columnOne@about': { template: 'column!' },
                'columnTwo@about': { 
                    templateUrl: 'views/table-data.html',
                    controller: 'controller'
                }
            }
            
        });*/
        
});

app.directive('sparkProduct', function() {
  return {
    restrict: 'AEC',
    templateUrl: 'views/directive/sparkproduct.html'
 }
});