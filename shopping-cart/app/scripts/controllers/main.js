'use strict';

/**
 * @ngdoc function
 * @name shoppingCartApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the shoppingCartApp
 */

app.controller('mainController',  ['$scope','masterDataFactory','$stateParams', function($scope,masterDataFactory,$stateParams){

	$scope.fProducts = masterDataFactory.getFormalProducts();
	$scope.cProducts = masterDataFactory.getCasualProducts();
	$scope.sProducts = masterDataFactory.getShoesProducts();
	$scope.aProducts = masterDataFactory.getAccessoriesProducts();
	$scope.allProducts = masterDataFactory.getAllProducts();

	$scope.cartItems = [];
	$scope.addToCart = function(item){
		$scope.totalPrice = 0;	
		$scope.cartItems.push(item);
		angular.forEach($scope.cartItems,function(i){
			$scope.totalPrice = $scope.totalPrice + i.price;
		});
	};

	$scope.dummyText = 'Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet  '+
						'consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et '+
						'quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula '+
						'sem ut volutpat.Et harum quidem rerum facilis est et expedita distinctio lorem ipsum '+
						'dolor sit amet                                                                       '+
						'consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et '+
						'quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula '+
						'sem ut volutpat.Et harum quidem rerum facilis est et expedita distinctio lorem ipsum '+
						'dolor sit amet                                                                       '+
						'consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et '+
						'quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula '+
						'sem ut volutpat.                                                                     ';

	$scope.currentProduct = $stateParams.product;
	

}]);

app.controller('cartController', ['$scope','localStoreService', function($scope,localStoreService){
	    $scope.invoice = {
        items: $scope.$parent.cartItems
    };

    $scope.removeItem = function(index) {
        $scope.invoice.items.splice(index, 1);
    };

    $scope.total = function() {
        var total = 0;
        angular.forEach($scope.invoice.items, function(item) {
            total += item.qty * item.price;
        })

        return total;
    };
    $scope.confirm = function(){
    	var email = $scope.contact.email;
		if(localStoreService.isSupported){
			localStoreService.set(email,$scope.invoice.items);
		}
    };

    $scope.retrieveCart = function(){
    	if(localStoreService.isSupported){
			$scope.$parent.cartItems = localStoreService.get($scope.email);		}
    }


}]);

