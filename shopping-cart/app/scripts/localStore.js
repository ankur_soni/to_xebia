(function() {
'use strict';
var angularLocalStorage = angular.module('localStore', []);

angularLocalStorage.provider('localStoreService', function() {

this.prefix = '_localStore';

this.storageType = 'localStorage';

 this.notify = {
    setItem: true,
    removeItem: false
};
 
  // Setter for the prefix
  this.setPrefix = function(prefix) {
    this.prefix = prefix;
  };

   // Setter for the storageType
   this.setStorageType = function(storageType) {
       this.storageType = storageType;
   };
  this.setNotify = function(itemSet, itemRemove) {
    this.notify = {
      setItem: itemSet,
      removeItem: itemRemove
    };
  };

  this.$get = ['$rootScope', '$window', '$document', function($rootScope, $window, $document) {
    var self = this;
    var prefix = self.prefix;
    var notify = self.notify;
    var storageType = self.storageType;
    var webStorage;

    // When Angular's $document is not available
    if (!$document) {
      $document = document;
    } else if ($document[0]) {
      $document = $document[0];
    }

    // If there is a prefix set in the config lets use that with an appended period for readability
    if (prefix.substr(-1) !== '.') {
      prefix = !!prefix ? prefix + '.' : '';
    }
    var deriveQualifiedKey = function(key) {
      return prefix + key;
    };
    // Checks the browser to see if local storage is supported
    var browserSupportsLocalStorage = (function () {
    	
      try {
        var supported = (storageType in $window && $window[storageType] !== null);
        var key = deriveQualifiedKey('_shoppingcart');
        if (supported) {
          webStorage = $window[storageType];
          webStorage.setItem(key, '');
          webStorage.removeItem(key);
        }

        return supported;
      } catch (e) {
        //$rootScope.$broadcast('localStore.notification.error', e.message);
        return false;
      }
    }());

    var addToLocalStorage = function (key, value) {
    	if (typeof value === "undefined") {
            value = null;
          }
          try {
            if (angular.isObject(value) || angular.isArray(value)) {
              value = angular.toJson(value);
            }
            if (webStorage) {webStorage.setItem(deriveQualifiedKey(key), value)};
            if (notify.setItem) {
            //un-comment lines with .$broadcast if logging is required..
            // if broadcasting need to listen at some point also
             // $rootScope.$broadcast('localStore.notification.setitem', {key: key, newvalue: value, storageType: self.storageType});
            }
          } catch (e) {
            //$rootScope.$broadcast('localStore.notification.error', e.message);
            return false;
          }
          return true;
        
      };
    var getFromLocalStorage = function (key) {
    	var item = webStorage ? webStorage.getItem(deriveQualifiedKey(key)) : null;
        // angular.toJson will convert null to 'null', so a proper conversion is needed
        if (!item || item === 'null') {
          return null;
        }

        if (item.charAt(0) === "{" || item.charAt(0) === "[") {
          return angular.fromJson(item);
        }

        return item;
	
        };
    var removeFromLocalStorage = function (key) {
	 try {
	        webStorage.removeItem(deriveQualifiedKey(key));
	        if (notify.removeItem) {
	          //$rootScope.$broadcast('localStore.notification.removeitem', {key: key, storageType: self.storageType});
	        }
	      } catch (e) {
	        //$rootScope.$broadcast('localStore.notification.error', e.message);
	        return false;
	      }
	      return true;
    };
var clearAllFromLocalStorage = function (regularExpression) {
	regularExpression = regularExpression || "";
    //accounting for the '.' in the prefix when creating a regex
    var tempPrefix = prefix.slice(0, -1);
    var testRegex = new RegExp(tempPrefix + '.' + regularExpression);
    var prefixLength = prefix.length;

    for (var key in webStorage) {
      // Only remove items that are for this app and match the regular expression
      if (testRegex.test(key)) {
        try {
          removeFromLocalStorage(key.substr(prefixLength));
        } catch (e) {
         // $rootScope.$broadcast('localStore.notification.error',e.message);
          return false;
        }
      }
    }
    return true;
};
    var getStorageType = function() {
      return storageType;
    };

    var bindToScope = function(scope, key, def) {
      var value = getFromLocalStorage(key);

      if (value === null && angular.isDefined(def)) {
        value = def;
      } else if (angular.isObject(value) && angular.isObject(def)) {
        value = angular.extend(def, value);
      }

      scope[key] = value;

      scope.$watchCollection(key, function(newVal) {
        addToLocalStorage(key, newVal);
      });
    };

    return {
      isSupported: browserSupportsLocalStorage,
      getStorageType: getStorageType,
      set: addToLocalStorage,
      add: addToLocalStorage, 
      get: getFromLocalStorage,
      remove: removeFromLocalStorage,
      clearAll: clearAllFromLocalStorage,
      bind: bindToScope,
      deriveKey: deriveQualifiedKey,
       };
  }];
});
}).call(this);