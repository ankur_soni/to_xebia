app.factory('masterDataFactory' , function(){

	var formalProducts = [
			{ name: 'formal_name1',price:11, code: 'code1',description:'description1 is for product in image',imgUrl:'images/formals/1.png' },
			{ name: 'formal_name2', price:9, code: 'code2',description:'description2 is for product in image',imgUrl:'images/formals/2.png' },
			{ name: 'formal_name3', price:4, code: 'code3',description:'description3 is for product in image',imgUrl:'images/formals/3.png' },
			{ name: 'formal_name4', price:27, code: 'code4',description:'description4 is for product in image',imgUrl:'images/formals/4.png' },
            { name: 'formal_name5', price:11, code: 'code5',description:'description5 is for product in image',imgUrl:'images/formals/5.png' },
			{ name: 'formal_name6', price:9, code: 'code6',description:'description6 is for product in image',imgUrl:'images/formals/6.png' },
			{ name: 'formal_name7', price:4, code: 'code7',description:'description7 is for product in image',imgUrl:'images/formals/7.png' },
			{ name: 'formal_name8', price:27, code: 'code8',description:'description8 is for product in image',imgUrl:'images/formals/8.png' },
           
		];

	var casualProducts = [
			{ name: 'casual_name1', price:11, code: 'code1',description:'description1 is for product in image',imgUrl:'images/casuals/1.png' },
			{ name: 'casual_name2', price:9, code: 'code2',description:'description2 is for product in image',imgUrl:'images/casuals/2.png' },
			{ name: 'casual_name3', price:4, code: 'code3',description:'description3 is for product in image',imgUrl:'images/casuals/3.png' },
			{ name: 'casual_name4', price:27, code: 'code4',description:'description4 is for product in image',imgUrl:'images/casuals/4.png' },
            { name: 'casual_name5', price:11, code: 'code5',description:'description5 is for product in image',imgUrl:'images/casuals/5.png' },
			{ name: 'casual_name6', price:9, code: 'code6',description:'description6 is for product in image',imgUrl:'images/casuals/6.png' },
			{ name: 'casual_name7', price:4, code: 'code7',description:'description7 is for product in image',imgUrl:'images/casuals/7.png' },
			{ name: 'casual_name8', price:27, code: 'code8',description:'description8 is for product in image',imgUrl:'images/casuals/8.png' },
           
		];

	var shoesProducts = [
			{ name: 'shoes_name1',price:11, code: 'code1',description:'description1 is for product in image',imgUrl:'images/shoes/1.png' },
			{ name: 'shoes_name2', price:9, code: 'code2',description:'description2 is for product in image',imgUrl:'images/shoes/2.png' },
			{ name: 'shoes_name3', price:4, code: 'code3',description:'description3 is for product in image',imgUrl:'images/shoes/3.png' },
			{ name: 'shoes_name4', price:27, code: 'code4',description:'description4 is for product in image',imgUrl:'images/shoes/4.png' },
            { name: 'shoes_name5',price:11, code: 'code5',description:'description5 is for product in image',imgUrl:'images/shoes/5.png' },
			{ name: 'shoes_name6', price:9, code: 'code6',description:'description6 is for product in image',imgUrl:'images/shoes/6.png' },
			{ name: 'shoes_name7', price:4, code: 'code7',description:'description7 is for product in image',imgUrl:'images/shoes/7.png' },
			{ name: 'shoes_name8', price:27, code: 'code8',description:'description8 is for product in image',imgUrl:'images/shoes/8.png' },
           
		];

	var accessoriesProducts = [
			{ name: 'accessories_name1',price:11, code: 'code1',description:'description1 is for product in image',imgUrl:'images/acc/1.png' },
			{ name: 'accessories_name2', price:9, code: 'code2',description:'description2 is for product in image',imgUrl:'images/acc/2.png' },
			{ name: 'accessories_name3', price:4, code: 'code3',description:'description3 is for product in image',imgUrl:'images/acc/3.png' },
			{ name: 'accessories_name4', price:27, code: 'code4',description:'description4 is for product in image',imgUrl:'images/acc/4.png' },
            { name: 'accessories_name5',price:11, code: 'code5',description:'description5 is for product in image',imgUrl:'images/acc/5.png' },
			{ name: 'accessories_name6', price:9, code: 'code6',description:'description6 is for product in image',imgUrl:'images/acc/6.png' },
			{ name: 'accessories_name7', price:4, code: 'code7',description:'description7 is for product in image',imgUrl:'images/acc/3.png' },
			{ name: 'accessories_name8', price:27, code: 'code8',description:'description8 is for product in image',imgUrl:'images/acc/2.png' },
           
		];

	var getFormalProducts = function(){
		return formalProducts;
	};

	var getCasualProducts = function(){
		return casualProducts;
	};

	var getShoesProducts = function(){
		return shoesProducts;
	};

	var getAccessoriesProducts = function(){
		return accessoriesProducts;
	};

	var getAllProducts = function(){
		return formalProducts.concat(casualProducts).concat(shoesProducts).concat(accessoriesProducts);
	};
	return {
		getFormalProducts : getFormalProducts,
		getCasualProducts : getCasualProducts,
		getShoesProducts : getShoesProducts,
		getAccessoriesProducts : getAccessoriesProducts,
		getAllProducts : getAllProducts
	};
});